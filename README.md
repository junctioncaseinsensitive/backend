== Setup Instructions ==

- cd to this dir and run in your term:

```
    composer install
```

- Edit the .env file to replace `DB_TABLE_PREFIX=imdumbanddidnotreplacemyname` with your name

- Run in your term

```
	php artisan migrate:install && php artisan migrate
```

- Serve

```
	php artisan serve
```

-- Start the queue

```
php artisan queue:work --queue=artists
php artisan queue:work --queue=offers
the node thingy
```
