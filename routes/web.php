<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('react');
});

Route::get('/loading', function () {
});

Route::get('/login', 'SpotifyController@login');
Route::get('/spotify_response', 'SpotifyController@spotifyResponse');
Route::get('/artists', 'SpotifyController@listArtists');
Route::get('/process/start', 'TripProcessController@start');
Route::get('/process/loading', 'TripProcessController@loading');
Route::get('/process/status', 'TripProcessController@status');
Route::get('/process/result', 'TripProcessController@result');
