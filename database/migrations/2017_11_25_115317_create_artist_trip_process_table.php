<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Many to many (ARtists / TripProcesses) jump table
 */
class CreateArtistTripProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artist_trip_process', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id');
            $table->integer('trip_process_id');
    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artist_trip_process');
    }
}
