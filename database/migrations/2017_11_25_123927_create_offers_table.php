<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 */
class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('outbound_price');
            $table->string('outbound_flightNumber');
            $table->string('outbound_departure_dateTime');
            $table->string('outbound_departure_locationCode');
            $table->string('outbound_departure_terminal')->nullable();
            $table->string('outbound_arrival_dateTime');
            $table->string('outbound_arrival_locationCode');
            $table->integer('outbound_duration');
            $table->string('inbound_price');
            $table->string('inbound_flightNumber');
            $table->string('inbound_departure_dateTime');
            $table->string('inbound_departure_locationCode');
            $table->string('inbound_departure_terminal')->nullable();
            $table->string('inbound_arrival_dateTime');
            $table->string('inbound_arrival_locationCode');
            $table->integer('inbound_duration');
            $table->string('redirectUrl');
            $table->char('hash', 64)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
