<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_processes', function (Blueprint $table) {
            $table->increments('id');
            $table->char('hash', 32)->unique();
            $table->string('spotify_token');
            $table->string('location_lat');
            $table->string('location_long');
            $table->boolean('artists')->default(false);
            $table->boolean('events')->default(false);
            $table->boolean('offers')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_processes');
    }
}
