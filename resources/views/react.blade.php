<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700&amp;subset=cyrillic" rel="stylesheet">
    <title>Make concerts great again!</title>
    <meta name="theme-color" content="#5890E8" />
    <link rel="manifest" href="./manifest.f08939c921bdf5a34545fa4cfbf6f5a8.json" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link href="./main.css?0_e9928b35342716e3c4ef38afa3a9e73d" rel="stylesheet">
</head>
<body>
<div id="root">
    <div class="preloader-page">
        <div class="preloader">
            <div class="preloader-spin"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="./bundle.js"></script></body>
</html>