<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            <div class="content">
                @forelse ($artists as $artist)
                    <div style="padding: 16px; border: 1px solid grey; margin: 16px">
                        <img src="{{ $artist->image_url }}" style="max-width:100px;max-height: 100px;"/>
                        <br/>
                        <span style="font-weight:bold;margin-top:8px;display:block">{{ $artist->name }}</span>
                    </div>
                @empty
                    <div style="padding: 24px; border: 1px solid grey; margin: 16px; font-size: 120%">
                        <span style="font-weight:bold;display:block">No artists found.</span>
                        <br>
                        <span style="display:block">Listen some more music!</span>
                    </div>
                @endforelse
            </div>
        </div>
    </body>
</html>
