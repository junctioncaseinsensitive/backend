<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ArtistTripProcess
 *
 * @property int $id
 * @property int $artist_id
 * @property int $trip_process_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ArtistTripProcess whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ArtistTripProcess whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ArtistTripProcess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ArtistTripProcess whereTripProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ArtistTripProcess whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArtistTripProcess extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'artist_trip_process';

    /**
     * @param int $artistId
     * @param int $processId
     * @return ArtistTripProcess
     */
    public static function insert($artistId, $processId)
    {
        $atp = new ArtistTripProcess();
        $atp->artist_id = $artistId;
        $atp->trip_process_id = $processId;
        $atp->save();
        return $atp;
    }
}
