<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Offer
 *
 * @property int $id
 * @property string $outbound_price
 * @property string $outbound_flightNumber
 * @property string $outbound_departure_dateTime
 * @property string $outbound_departure_locationCode
 * @property string $outbound_departure_terminal
 * @property string $outbound_arrival_dateTime
 * @property string $outbound_arrival_locationCode
 * @property int $outbound_duration
 * @property string $inbound_price
 * @property string $inbound_flightNumber
 * @property string $inbound_departure_dateTime
 * @property string $inbound_departure_locationCode
 * @property string $inbound_departure_terminal
 * @property string $inbound_arrival_dateTime
 * @property string $inbound_arrival_locationCode
 * @property int $inbound_duration
 * @property string $redirectUrl
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $hash
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundArrivalDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundArrivalLocationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundDepartureDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundDepartureLocationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundDepartureTerminal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundFlightNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereInboundPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundArrivalDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundArrivalLocationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundDepartureDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundDepartureLocationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundDepartureTerminal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundFlightNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOutboundPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereRedirectUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Offer whereOffer($value)
 * @mixin \Eloquent
 */
class Offer extends Model
{
    /**
     * @var string
     */
    protected $table = 'offers';

    protected function event()
    {
        return $this->belongsTo('App\Event');
    }

    /**
     * @var string[]
     */
    protected $fillable = [
        'outbound_price',
        'outbound_flightNumber',
        'outbound_departure_dateTime',
        'outbound_departure_locationCode',
        'outbound_departure_terminal',
        'outbound_arrival_dateTime',
        'outbound_arrival_locationCode',
        'outbound_duration',
        'inbound_price',
        'inbound_flightNumber',
        'inbound_departure_dateTime',
        'inbound_departure_locationCode',
        'inbound_departure_terminal',
        'inbound_arrival_dateTime',
        'inbound_arrival_locationCode',
        'inbound_duration',
        'redirectUrl',
        'hash',
        'event_id',
    ];
}
