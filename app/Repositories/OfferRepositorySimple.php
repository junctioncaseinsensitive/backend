<?php
namespace App\Repositories;

use App\Offer;

/**
 */
class OfferRepositorySimple implements OfferRepository
{
    /**
     * @param mixed[] $offers
     */
    public function saveOffers(array $offers)
    {
        foreach ($offers as $offer) {
            $this->saveOffer($offer);
        }
    }

    /**
     * @param mixed[] $offer
     */
    public function saveOffer(array $offer)
    {
        $offerFlat = $this->flatten($offer);

        if (!Offer::whereHash($offer['hash'])->exists()) {
            //$offerFlat['event_id'] exists and is properly set
            $offerModel = new Offer($offerFlat);
            //$offerModel->event_id does not
            $offerModel->save();
        }
    }

    /**
     * @param mixed|mixed[] $object
     * @param string $key
     * @param mixed|mixed[] $objectFlattened
     *
     * @return mixed[]
     */
    private function flatten($object, string $key = '', &$objectFlattened = [])
    {
        if (!is_array($object)) {
            return $objectFlattened[$key] = $object;
        }

        foreach ($object as $nextKey => $value) {
            $newKey = $key;

            if (empty($newKey)) {
                $newKey = $nextKey;
            } else {
                $newKey .= '_' . $nextKey;
            }

            $this->flatten($value, $newKey, $objectFlattened);
        }

        return $objectFlattened;
    }
}
