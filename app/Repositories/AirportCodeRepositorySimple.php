<?php
namespace App\Repositories;

/**
 */
class AirportCodeRepositorySimple implements AirportCodeRepository
{
    /**
     */
    const PATH_AIRPORT_DATA = __DIR__ . '/../../resources/data/airports.json';

    /**
     * @param float $latitude
     * @param float $longitude
     *
     * @return string
     */
    public function getAirportCode(float $latitude, float $longitude): string
    {
        $airportDataJson = file_get_contents(self::PATH_AIRPORT_DATA);
        $airportData = \GuzzleHttp\json_decode($airportDataJson, true);
        $bestCode = null;
        $bestDistance = null;

        foreach ($airportData as $code => $coordinates) {
            $distance = $this->getDistance($latitude, $longitude, $coordinates[0], $coordinates[1]);

            if (is_null($bestCode) || $distance < $bestDistance) {
                $bestCode = $code;
                $bestDistance = $distance;
            }
        }

        return $bestCode;
    }

    /**
     * @param float $lat0
     * @param float $lon0
     * @param float $lat1
     * @param float $lon1
     *
     * @return float
     */
    private function getDistance(float $lat0, float $lon0, float $lat1, float $lon1): float
    {
        return sqrt(pow($lat1 - $lat0, 2) + pow($lon1 - $lon0, 2));
    }
}
