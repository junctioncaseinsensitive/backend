<?php
namespace App\Repositories;

use GuzzleHttp\Client;

/**
 */
class FinnairApiRepositorySimple implements FinnairApiRepository
{
    /**
     * Field constants.
     */
    const FIELD_ADULTS = 'adults';
    const FIELD_CHILDREN = 'children';
    const FIELD_INFANTS = 'infants';
    const FIELD_LOCALE = 'locale';
    const FIELD_DEPARTURE_LOCATION_CODE = 'departureLocationCode';
    const FIELD_ORIGIN = 'origin';
    const FIELD_DESTINATION_LOCATION_CODE = 'destinationLocationCode';
    const FIELD_DESTINATION = 'destination';
    const FIELD_DEPARTURE_DATE = 'departureDate';
    const FIELD_RETURN_DATE = 'returnDate';
    const FIELD_CABIN = 'cabin';

    /**
     * Guzzle option fields.
     */
    const GUZZLE_OPTION_FIELD_BASE_URI = 'base_uri';

    /**
     * Base URI of Junction's version of Finnair Offer API.
     */
    const URI_BASE_JUNCTION_FINNAIR_OFFER_API = 'https://offer-junction.ecom.finnair.com/api/offerList';
    const URI_BASE_FINNAIR_BOOKING = 'https://beta.finnair.com/en/booking/flight-selection';

    /**
     * Default values for details.
     */
    const ADULT_COUNT = '1';
    const COUNT_NONE = '0';
    const CHILDREN_COUNT = self::COUNT_NONE;
    const INFANT_COUNT = self::COUNT_NONE;
    const LOCALE_EN = 'en';
    const LOCALE_DEFAULT = self::LOCALE_EN;
    const CABIN_ECONOMY = 'ECONOMY';

    /**
     * Separator for URI query.
     */
    const SEPARATOR_QUERY = '?';

    /**
     * @var Client $client
     */
    private $client;

    /**
     */
    public function __construct()
    {
        $this->client = new Client(
            [
                self::GUZZLE_OPTION_FIELD_BASE_URI => self::URI_BASE_JUNCTION_FINNAIR_OFFER_API,
            ]
        );
    }

    /**
     * @param string $departureCode
     * @param string $destinationCode
     * @param string $eventDate
     * @param string $eventId
     *
     * @return mixed[]
     */
    public function getOffers(
        string $departureCode,
        string $destinationCode,
        string $eventDate,
        string $eventId
    ): array {
        $departureDate = $this->subDay($eventDate);
        $returnDate = $this->addDay($eventDate);

        $params = [
            self::FIELD_ADULTS => self::ADULT_COUNT,
            self::FIELD_LOCALE => self::LOCALE_DEFAULT,
            self::FIELD_DEPARTURE_LOCATION_CODE => $departureCode,
            self::FIELD_DESTINATION_LOCATION_CODE => $destinationCode,
            self::FIELD_DEPARTURE_DATE => $departureDate,
            self::FIELD_RETURN_DATE => $returnDate,
        ];
        $bodyJsonString = $this->client->get(self::SEPARATOR_QUERY . http_build_query($params))->getBody();
        $finnairOfferApiResponseBody = \GuzzleHttp\json_decode($bodyJsonString, true);
        $offers = $this->fetchOffers($finnairOfferApiResponseBody);

        if (is_array($offers) && count($offers) > 0) {
            $offers = [reset($offers)];
        }

        $offersFiltered = $this->filterOffers($offers);
        $offersExtended = $this->extendOffersWithEventId(
            $offersFiltered,
            $eventId
        );
        $offersExtended = $this->extendOffersFromItineraries($offersExtended, $finnairOfferApiResponseBody);
        $offersComplete = $this->extendOffersWithRedirectUris(
            $offersExtended,
            $departureCode,
            $destinationCode,
            $departureDate,
            $returnDate
        );

        return $offersComplete;
    }

    /**
     * @param string $date
     *
     * @return string
     */
    private function subDay(string $date): string
    {
        $dateTime = \DateTime::createFromFormat('Y-m-d', $date);
        $dateTime = $dateTime->sub(new \DateInterval('P1D'));

        return $dateTime->format('Y-m-d');
    }

    /**
     * @param string $date
     *
     * @return string
     */
    private function addDay(string $date): string
    {
        $dateTime = \DateTime::createFromFormat('Y-m-d', $date);
        $dateTime = $dateTime->add(new \DateInterval('P1D'));

        return $dateTime->format('Y-m-d');
    }

    /**
     * @param mixed[] $responseBody
     *
     * @return mixed[]
     */
    private function fetchOffers(array $responseBody): array
    {
        if (isset($responseBody['offers'])) {
            return $responseBody['offers'];
        } else {
            return [];
        }
    }

    /**
     * @param mixed[] $offers
     *
     * @return mixed[]
     */
    private function filterOffers(array $offers): array
    {
        $offersSplit = [];

        foreach ($offers as $offer) {
            $offersSplit[] = $this->filterOffer($offer);
        }

        return $offersSplit;
    }

    /**
     * @param string[] $offer
     *
     * @return mixed[]
     */
    private function filterOffer(array $offer): array
    {
        $offerFiltered = [
            'outbound' => [
                'id' => $offer['outboundId'],
                'price' => $offer['outboundPrice'],
            ],
            'inbound' => [
                'id' => $offer['inboundId'],
                'price' => $offer['inboundPrice'],
            ],
        ];

        return $offerFiltered;
    }

    /**
     * @param mixed[] $offers
     * @param string $event_id
     *
     * @return mixed[]
     */
    private function extendOffersWithEventId(
        array $offers,
        string $event_id
    ): array {
        $offersExtended = [];

        foreach ($offers as $offer) {
            $offer['event_id'] = $event_id;
            $offersExtended[] = $offer;
        }

        return $offersExtended;
    }

    /**
     * @param mixed[] $offers
     * @param mixed[] $responseBody
     *
     * @return mixed[]
     */
    private function extendOffersFromItineraries(array $offers, array $responseBody): array
    {
        $offersExtended = [];

        foreach ($offers as $offer) {
            $offerExtended = $this->extendOffer($offer, $responseBody);
            $hash = $this->calculateHash($offerExtended);

            if (!isset($offersExtended[$hash])) {
                $offerExtended['hash'] = $hash;
                $offersExtended[$hash] = $offerExtended;
            }
        }

        return array_values($offersExtended);
    }

    /**
     * @param mixed[] $offer
     * @param mixed[] $responseBody
     *
     * @return mixed[]
     */
    private function extendOffer(array $offer, array $responseBody): array
    {
        $offer = $this->extendOfferFromItineraryForOneDirection($offer, $responseBody, 'inbound');
        $offer = $this->extendOfferFromItineraryForOneDirection($offer, $responseBody, 'outbound');

        return $offer;
    }

    /**
     * @param mixed[] $offer
     * @param mixed[] $responseBody
     * @param string $direction
     *
     * @return mixed[]
     */
    private function extendOfferFromItineraryForOneDirection(
        array $offer,
        array $responseBody,
        string $direction
    ): array {
        $id = $offer[$direction]['id'];
        $itinerary = $this->getItineraryFull($responseBody, $direction, $id);
        $offer[$direction]['flightNumber'] = $itinerary['flightNumber'];
        $offer[$direction]['departure'] = $itinerary['departure'];
        $offer[$direction]['arrival'] = $itinerary['arrival'];
        $offer[$direction]['duration'] = (int)($itinerary['duration']['milliseconds'] / 1000);
        unset($offer[$direction]['id']);

        return $offer;
    }

    /**
     * @param mixed[] $responseBody
     * @param string $direction
     * @param string $id
     *
     * @return array
     */
    private function getItineraryFull(
        array $responseBody,
        string $direction,
        string $id
    ): array {
        $itineraries = $responseBody[$direction . 's'][$id]['itinerary'];
        $itineraryFirst = $itineraries[0];

        if (count($itineraries) > 2) {
            $itinerarySecond = $itineraries[1];
            $itineraryThird = $itineraries[2];
            $itineraryFirst['flightNumber'] .= '/' . $itineraryThird['flightNumber'];
            $itineraryFirst['duration']['milliseconds'] =
                (int)$itineraryFirst['duration']['milliseconds'] +
                (int)$itinerarySecond['duration']['milliseconds'] +
                (int)$itineraryThird['duration']['milliseconds'];
            $itineraryFirst['arrival'] = $itineraryThird['arrival'];
        }

        return $itineraryFirst;
    }

    /**
     * @param string[] $offer
     *
     * @return string
     */
    private function calculateHash(array $offer): string
    {
        $outboundHashBase = $offer['outbound']['flightNumber'] . '|' . $offer['outbound']['departure']['dateTime'];
        $inboundHashBase = $offer['inbound']['flightNumber'] . '|' . $offer['inbound']['departure']['dateTime'];
        $hashBase = $outboundHashBase . '|' . $inboundHashBase . '|' . $offer['event_id'];

        return hash('SHA256', $hashBase);
    }

    /**
     * @param string[] $offers
     * @param string $departureCode
     * @param string $destinationCode
     * @param string $departureDate
     * @param string $returnDate
     *
     * @return string[]
     */
    private function extendOffersWithRedirectUris(
        array $offers,
        string $departureCode,
        string $destinationCode,
        string $departureDate,
        string $returnDate
    ): array {
        $offersExtended = [];

        foreach ($offers as $offer) {
            $offersExtended[] = $this->extendOfferWithRedirectUri(
                $offer,
                $departureCode,
                $destinationCode,
                $departureDate,
                $returnDate
            );
        }

        return $offersExtended;
    }

    /**
     * @param mixed[] $offer
     * @param string $departureCode
     * @param string $destinationCode
     * @param string $departureDate
     * @param string $returnDate
     *
     * @return mixed[]
     */
    private function extendOfferWithRedirectUri(
        array $offer,
        string $departureCode,
        string $destinationCode,
        string $departureDate,
        string $returnDate
    ): array {
        $params = [
            self::FIELD_ORIGIN => $departureCode,
            self::FIELD_DESTINATION => $destinationCode,
            self::FIELD_DEPARTURE_DATE => $departureDate,
            self::FIELD_RETURN_DATE => $returnDate,
            self::FIELD_ADULTS => self::ADULT_COUNT,
            self::FIELD_CHILDREN => self::CHILDREN_COUNT,
            self::FIELD_INFANTS => self::INFANT_COUNT,
            self::FIELD_CABIN => self::CABIN_ECONOMY,
        ];
        $queryString = self::SEPARATOR_QUERY . http_build_query($params);

        $offer['redirectUrl'] = self::URI_BASE_FINNAIR_BOOKING . $queryString;

        return $offer;
    }
}
