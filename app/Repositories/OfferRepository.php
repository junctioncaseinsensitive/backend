<?php
namespace App\Repositories;

/**
 */
interface OfferRepository
{
    /**
     * @param mixed[] $offers
     */
    public function saveOffers(array $offers);

    /**
     * @param mixed[] $offer
     */
    public function saveOffer(array $offer);
}
