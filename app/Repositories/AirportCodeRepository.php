<?php
namespace App\Repositories;

/**
 */
interface AirportCodeRepository
{
    /**
     * @param float $latitude
     * @param float $longitude
     *
     * @return string
     */
    public function getAirportCode(float $latitude, float $longitude): string;
}
