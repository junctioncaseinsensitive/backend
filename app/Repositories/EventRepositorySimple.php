<?php
namespace App\Repositories;

use App\Event;
use Carbon\Carbon;

/**
 */
class EventRepositorySimple implements EventRepository
{
    /**
     * @param int $processId
     *
     * @return Event[]
     */
    public function getAllByProcessId(int $processId)
    {
        $dateTomorrow =  Carbon::tomorrow()->format('Y-m-d');

        return Event::whereProcessId($processId)->where('date', '>', $dateTomorrow)->get();
    }
}
