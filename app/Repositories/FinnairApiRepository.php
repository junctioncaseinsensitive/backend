<?php
namespace App\Repositories;

/**
 */
interface FinnairApiRepository
{
    /**
     * @param string $departureCode
     * @param string $destinationCode
     * @param string $eventDate
     * @param string $eventId
     *
     * @return mixed[]
     */
    public function getOffers(
        string $departureCode,
        string $destinationCode,
        string $eventDate,
        string $eventId
    ): array;
}
