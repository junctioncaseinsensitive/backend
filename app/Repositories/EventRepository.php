<?php
namespace App\Repositories;

/**
 */
interface EventRepository
{
    /**
     * @param int $processId
     */
    public function getAllByProcessId(int $processId);
}
