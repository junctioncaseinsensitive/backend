<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Artist
 *
 * @property int $id
 * @property string $name
 * @property string|null $image_url
 * @property string|null $spotify_id
 * @property string|null $bandsintown_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $spotify_user_id
 * @property int $spotify_user_ordering
 * @method static Builder|Artist userArtists($spotifyId)
 * @method static Builder|Artist whereBandsintownId($value)
 * @method static Builder|Artist whereCreatedAt($value)
 * @method static Builder|Artist whereId($value)
 * @method static Builder|Artist whereImageUrl($value)
 * @method static Builder|Artist whereName($value)
 * @method static Builder|Artist whereSpotifyId($value)
 * @method static Builder|Artist whereSpotifyUserId($value)
 * @method static Builder|Artist whereSpotifyUserOrdering($value)
 * @method static Builder|Artist whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TripProcess[] $tripProcesses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\TripProcess[] $tripProcess
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Event[] $event
 */
class Artist extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'artists';

    public function tripProcess()
    {
        return $this->belongsToMany('App\TripProcess')->withTimestamps();
    }

    public function event()
    {
        return $this->hasMany('App\Event');
    }

    public static function insert(string $name, string $imageUrl, string $spotifyId)
    {
        $artist = new Artist();
        $artist->name = $name;
        $artist->image_url = $imageUrl;
        $artist->spotify_id = $spotifyId;
        $artist->save();

        return $artist;
    }
}
