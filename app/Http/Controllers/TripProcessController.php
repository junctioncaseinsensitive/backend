<?php
namespace App\Http\Controllers;

use App\Jobs\ProcessOffers;
use App\Jobs\ProcessArtists;
use App\TripProcess;
use App\Event;
use App\Offer;
use Illuminate\Http\Request;

class TripProcessController extends Controller
{
    public function start(Request $request)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $request->validate([
            'spotify_token' => 'required',
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
        ]);

        $tripProcess = new TripProcess;
        $tripProcess->spotify_token = $request->input('spotify_token');
        $tripProcess->location_lat = $request->input('lat');
        $tripProcess->location_long = $request->input('long');
        $tripProcess->save();

        ProcessArtists::dispatch($tripProcess->id)->onQueue('artists');
        ProcessOffers::dispatch($tripProcess->id)->onQueue('offers');

        return redirect('/process/loading?process_id=' . $tripProcess->hash);
    }

    public function loading()
    {
        return view('loading');
    }

    public function status(Request $request)
    {
        $tripProcess = TripProcess::where('hash', $request->input('process_id'))->first();

        if(!$tripProcess) {
            abort(404);
        }

        return json_encode([
            'artists' => (bool)$tripProcess->artists,
            'concerts' => (bool)$tripProcess->events,
            'flights' => (bool)$tripProcess->offers,
        ]);
    }

    public function result(Request $request)
    {
        $tripProcess = TripProcess::where('hash', $request->input('process_id'))
            ->first();

        if (!$tripProcess) {
            abort(404);
        }

        $events = Event::with(['artist'])->where('process_id', $tripProcess->id)->get();
        $countPerArtist = [];
        $collection = [];

        foreach($events as $event) {
            $eventForCollection = $this->getEventForCollection($event);
            $countPerArtist[$event->artist_id] = (isset($countPerArtist[$event->artist_id]))
                ? $countPerArtist[$event->artist_id] + 1
                : 1;

            if ($eventForCollection && $countPerArtist[$event->artist_id] <= 5) {
                $collection[] = $eventForCollection;
            }
        }


        return json_encode([
            'collection' => $collection,
            'meta' => [
                'sorting' => 'time',
            ],
        ]);
    }

    /**
     * @param Event $event
     *
     * @return mixed[]|bool
     */
    private function getEventForCollection(Event $event) {
        /* @var Event $event */
        $offer = Offer::where('event_id', $event->id)->first();

        if (!$offer) {
            return false;
        }

        $result = [
            'artist' => [
                'name' => $event->artist->name,
                'photo_url' => $event->artist->image_url,
            ],
            'event' => [
                'id' => $event->id,
                'destination' => $event->city_name,
                'date' => $event->date,
                'name' => $event->name,
                'image' => $event->artist->image_url,
            ],
            'trip' => [
                'flights' => [
                    'inbound' => [
                        'price' => $offer->inbound_price,
                        'flight_number' => $offer->inbound_flightNumber,
                        'departure_time' => $this->formatTime($offer->inbound_departure_dateTime),
                        'departure_airport' => $offer->inbound_departure_locationCode,
                        'arrival_time' => $this->formatTime($offer->inbound_arrival_dateTime),
                        'arrival_airport' => $offer->inbound_arrival_locationCode,
                    ],
                    'outbound' => [
                        'price' => $offer->outbound_price,
                        'flight_number' => $offer->outbound_flightNumber,
                        'departure_time' => $this->formatTime($offer->outbound_departure_dateTime),
                        'departure_airport' => $offer->outbound_departure_locationCode,
                        'arrival_time' => $this->formatTime($offer->outbound_arrival_dateTime),
                        'arrival_airport' => $offer->outbound_arrival_locationCode,
                    ],
                ],
                'redirect_url' => $offer->redirectUrl,
            ],
            'price' => $offer->inbound_price + $offer->outbound_price,
        ];

        return $result;
    }

    /**
     * @param string $time
     *
     * @return int
     */
    private function formatTime(string $time): int {
        $dateTime = new \DateTime($time);

        return (integer) $dateTime->format('U');
    }
}
