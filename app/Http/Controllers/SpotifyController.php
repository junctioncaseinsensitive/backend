<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

class SpotifyController extends Controller
{
    /* @var SpotifyWebAPI */
    private $api;
    /* @var Session */
    private $session;

    public function __construct()
    {
        $this->middleware(function (Request $request, callable $next) {
            $this->api = new SpotifyWebAPI();
            $this->session = new Session(
                config('app.spotify.client_id'),
                config('app.spotify.client_secret'),
                'http://' . $_SERVER['SERVER_NAME'] . '/spotify_response'
            );

            if ($this->shouldRedirectToSpotifyLogin($request))
                return redirect('/login');

            $this->api->setAccessToken($request->session()->get('spotify_token'));

            return $next($request);
        });
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function shouldRedirectToSpotifyLogin(Request $request): bool
    {
        return !$request->session()->has('spotify_token')
            && $request->route()->getActionMethod() !== 'login'
            && $request->route()->getActionMethod() !== 'spotifyResponse';
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     *
     * Update {@link shouldRedirectToSpotifyLogin()} if the method name changes.
     */
    public function login(Request $request)
    {
        $options = [
            'scope' => [
                'user-top-read',
            ],
            'state' => json_encode([
                'lat' => $request->input('lat'),
                'long' => $request->input('long'),
            ]),
        ];

        return redirect($this->session->getAuthorizeUrl($options));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     *
     * Update {@link shouldRedirectToSpotifyLogin()} if the method name changes.
     */
    public function spotifyResponse(Request $request)
    {
        $authorizationCode = $request->get('code');
        $this->session->requestAccessToken($authorizationCode);

        $accessToken = $this->session->getAccessToken();
        $this->api->setAccessToken($accessToken);
        $request->session()->put('spotify_token', $accessToken);

        $state = json_decode($request->get('state'));

        return redirect()->action(
            'TripProcessController@start',
            [
                'lat' => $state->lat,
                'long' => $state->long,
                'spotify_token' => $accessToken,
            ]
        );
    }
}
