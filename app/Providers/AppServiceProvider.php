<?php
namespace App\Providers;

use App\Repositories\AirportCodeRepository;
use App\Repositories\AirportCodeRepositorySimple;
use App\Repositories\EventRepository;
use App\Repositories\EventRepositorySimple;
use App\Repositories\FinnairApiRepositorySimple;
use App\Repositories\FinnairApiRepository;
use App\Repositories\OfferRepository;
use App\Repositories\OfferRepositorySimple;
//use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;

/**
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FinnairApiRepository::class, FinnairApiRepositorySimple::class);
        $this->app->singleton(OfferRepository::class, OfferRepositorySimple::class);
        $this->app->singleton(AirportCodeRepository::class, AirportCodeRepositorySimple::class);
        $this->app->singleton(EventRepository::class, EventRepositorySimple::class);
    }
}
