<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TripProcess
 *
 * @property int $id
 * @property string $hash
 * @property string $spotify_token
 * @property string $city
 * @property int $artists
 * @property int $events
 * @property int $offers
 * @property string $location_lat
 * @property string $location_long
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Artist[] $artist
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Event[] $event
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Offer[] $offer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereArtists($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereConcerts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereFlights($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereSpotifyToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereEvents($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereLocationLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereLocationLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TripProcess whereOffers($value)
 * @mixin \Eloquent
 */
class TripProcess extends Model
{
    protected $table = 'trip_processes';

    public function artist()
    {
        return $this->hasManyThrough('App\Artist', 'App\ArtistTripProcess', 'trip_process_id', 'id', 'id', 'artist_id');
    }

    public function event()
    {
        return $this->hasManyThrough('App\Event', 'App\Artist', 'id', 'artist_id', 'c', 'id');
    }

    public function offer()
    {
        return [];
    }

    public function save(array $options = [])
    {
        if (empty($this->hash)) {
            $this->hash = md5(uniqid());
        }

        return parent::save();
    }
}
