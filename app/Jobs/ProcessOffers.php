<?php
namespace App\Jobs;

use App\Repositories\AirportCodeRepository;
use App\Repositories\EventRepository;
use App\Repositories\FinnairApiRepository;
use App\Repositories\OfferRepository;
use App\TripProcess;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 */
class ProcessOffers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int $processId
     */
    private $processId;

    /**
     * @param int $processId
     */
    public function __construct(int $processId)
    {
        $this->processId = $processId;
    }

    /**
     * @param EventRepository $eventRepository
     * @param AirportCodeRepository $airportCodeRepository
     * @param FinnairApiRepository $finnairApiRepository
     * @param OfferRepository $offerRepository
     *
     * @throws Exception
     */
    public function handle(
        EventRepository $eventRepository,
        AirportCodeRepository $airportCodeRepository,
        FinnairApiRepository $finnairApiRepository,
        OfferRepository $offerRepository
    ) {
        $process = TripProcess::find($this->processId);

        if ($process->events === 1) {
            $events = $eventRepository->getAllByProcessId($process->id);

            foreach ($events as $event) {
                try {
                    $departureCode = $airportCodeRepository->getAirportCode(
                        (float)$process->location_lat,
                        (float)$process->location_long
                    );
                    $destinationCode = $airportCodeRepository->getAirportCode(
                        (float)$event->lat,
                        (float)$event->lon
                    );
                    $offers = $finnairApiRepository->getOffers(
                        $departureCode,
                        $destinationCode,
                        $event->date,
                        $event->id
                    );
                    $offerRepository->saveOffers($offers);
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                }
            }

            $process->offers = 1;
            $process->save();
        } else {
            throw new Exception('Artists are not ready yet!');
        }
    }
}
