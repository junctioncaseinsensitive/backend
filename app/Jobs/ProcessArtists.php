<?php
namespace App\Jobs;

use App\Artist;
use App\ArtistTripProcess;
use App\TripProcess;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SpotifyWebAPI\SpotifyWebAPI;

/**
 */
class ProcessArtists implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var SpotifyWebAPI
     */
    private $api;

    /**
     * @var TripProcess
     */
    private $process;

    /**
     * @param int $processId
     */
    public function __construct(int $processId)
    {
        $this->process = TripProcess::find($processId);

        $this->api = new SpotifyWebAPI();
        $this->api->setAccessToken($this->process->spotify_token);
    }

    public function handle()
    {
        $userFollowedArtists = $this->api->getMyTop('artists', ['time_range' => 'short_term'])->items;

        foreach ($userFollowedArtists as $artist) {
            $artist = Artist::insert(
                $artist->name,
                $this->getBiggestImageUrl($artist->images),
                $artist->id
            );

            ArtistTripProcess::insert($artist->id, $this->process->id);
        }

        file_get_contents('http://localhost:3000/concert');

        $this->process->artists = true;
        $this->process->save();
    }

    /**
     * @param object[] $images
     *
     * @return string
     */
    private function getBiggestImageUrl(array $images): string
    {
        $biggest = 0;
        $biggestUrl = '';

        foreach ($images as $img) {
            if ($img->height > $biggest) {
                $biggestUrl = $img->url;
                $biggest = $img->height;
            }
        }

        return $biggestUrl;
    }
}
