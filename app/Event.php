<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Event
 *
 * @property int $id
 * @property string $lat
 * @property string $long
 * @property string $city_name
 * @property int $artist_id
 * @property string $date
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereArtistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Event whereProcessId($value)
 * @mixin \Eloquent
 * @property int $process_id
 */
class Event extends Model
{
    /**
     * @var string
     */
    protected $table = 'events';

    /**
     */
    public function artist()
    {
        return $this->belongsTo('App\Artist');
    }

    /**
     */
    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
}
